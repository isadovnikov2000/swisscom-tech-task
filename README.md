## Author notes

- Service require Docker at local machine
- Tests aren't finished
  - Here you can find some example of my tests with Kotlin https://github.com/isadounikau/phrase-client/
- Service has PULL and PUSH functionality for Device configurations
- The networks api performance problem solved by Spring Cache 
- Device Networks Relation saved in Guava Reloadable Cache
- Kotlin used as main language, because it was easier and faster to write stable code
- Spring Boot as main framework orchestrator, because it is overpowerfull production ready tool
- Service has established CI/CD pipeline and deployed to Heroku, because it is free 
- Jackson has been used as a parser, because on my opinion it has the best speed-usability balance 
- Feign client, because it is super simple to use


## Migration Tool Backend

## Overview
This service provide a functionality for Migration Tool

## Build, Test and Run
### Build
Run `mvn clean package`

### Tests
Run with unit test only `mvn clean test`
Run with all test `mvn clean verify`

### Run locally
* Before launching the application recommended running `docker-compose.yml` file
* Run `mvn spring-boot:run`


## How to use

### Api 
Available API calls schema provided by Swagger

    http://localhost:8080/swagger-ui.html
    https://isadounikau-migration-tool.herokuapp.com/swagger-ui.html
    

## Dependencies

- Wiremock service

## Continuous Integration
Provided by Bitbucket Pipelines
Configuration File : `bitbucket-pipelines.yml`

## Continuous Deployment
Provided by Bitbucket Pipelines and Heroku
Configuration File : `bitbucket-pipelines.yml`

## Configuration
Configuration File : `src/main/resources/application.yml`

## TODO
- Add UI
- Add user management system
- Add client restrictions
- Add cloud configuration support
