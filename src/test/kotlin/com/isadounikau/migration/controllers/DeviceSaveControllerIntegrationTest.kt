package com.isadounikau.migration.controllers

import com.isadounikau.migration.AbstractIntegrationTest
import org.junit.Test
import org.springframework.http.MediaType
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.MediaType.TEXT_PLAIN_VALUE
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.UUID

//I would like to use Spock but
class DeviceSaveControllerIntegrationTest : AbstractIntegrationTest() {

    @Test
    fun givenDevice_whenSaveJsonDevice_thenResponseStatusIsOk() {
        //GIVEN

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .put("/api/v1/devices")
                .content("""
                    {
                       "deviceId": "id11",
                       "param1": "value11_1",
                       "param2": "value11_2"
                    }
                """.trimIndent())
                .contentType(APPLICATION_JSON_VALUE)
        )

        //THEN
        resultActions.andExpect(status().isOk)
    }

    @Test
    fun givenDevice_whenSaveTextDevice_thenResponseStatusIsOk() {
        //GIVEN

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .put("/api/v1/devices")
                .content("""
                    uuid=uuid23
                    param1=value23_1
                    param2=value23_2
                    param3=value23_3
                """.trimIndent())
                .contentType(TEXT_PLAIN_VALUE)
        )

        //THEN
        resultActions.andExpect(status().isOk)
    }

    @Test
    fun givenDevice_whenSaveUnsupportedDevice_thenResponseStatusIsUnsupportedMediaType() {
        //GIVEN

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .put("/api/v1/devices")
                .content(UUID.randomUUID().toString().toByteArray())
                .contentType(MediaType.IMAGE_PNG)
        )

        //THEN
        resultActions.andExpect(status().isUnsupportedMediaType)
    }


}
