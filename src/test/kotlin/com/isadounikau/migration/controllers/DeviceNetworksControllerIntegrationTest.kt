package com.isadounikau.migration.controllers

import com.isadounikau.migration.AbstractIntegrationTest
import org.junit.Test
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

//I would like to use Spock but I didn't have enough free time, also tests don't pass my acceptance criteria
//The time constraint is the main reason to leave it unfinished
class DeviceNetworksControllerIntegrationTest : AbstractIntegrationTest() {

    @Test
    fun givenDeviceId_whenDeviceNetworksExist_thenResponseStatusIsOk() {
        //GIVEN

        //WHEN
        val resultActions = mvc.perform(
            MockMvcRequestBuilders
                .get("/api/v1/devices/id11/networks")
        )

        //THEN
        resultActions.andExpect(status().isOk)
        //TODO compare response body
    }

}
