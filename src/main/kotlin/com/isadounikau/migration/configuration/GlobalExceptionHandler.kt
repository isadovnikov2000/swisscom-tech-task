package com.isadounikau.migration.configuration

import com.isadounikau.migration.clients.devices.DeviceNotFoundException
import com.isadounikau.migration.controllers.mappers.MoreThenOneDeviceMapperFoundException
import com.isadounikau.migration.controllers.mappers.NetworkMapperNotFoundException
import com.isadounikau.migration.controllers.mappers.NoDeviceMapperFoundException
import com.isadounikau.migration.services.devices.NetworkNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.OffsetDateTime

@ControllerAdvice
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [
        IllegalArgumentException::class,
        MoreThenOneDeviceMapperFoundException::class,
        NoDeviceMapperFoundException::class,
        NetworkMapperNotFoundException::class
    ])
    fun handleIllegalArgumentException(e: Exception): ResponseEntity<ErrorMessage> {
        return ResponseEntity(logAndConvert(e), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(value = [NetworkNotFoundException::class, DeviceNotFoundException::class])
    fun handleEntityNotFoundException(e: Exception): ResponseEntity<ErrorMessage> {
        return ResponseEntity(logAndConvert(e), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(value = [ServerException::class, Throwable::class])
    fun handleInternalServerError(e: Exception): ResponseEntity<ErrorMessage> {
        return ResponseEntity(logAndConvert(e), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    private fun getRequestUri(): String {
        if (RequestContextHolder.currentRequestAttributes() is ServletRequestAttributes) {
            return (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request.requestURI
        }
        return ""
    }

    private fun logAndConvert(throwable: Throwable?): ErrorMessage {
        logger.error("Error catch by the GlobalExceptionHandler", throwable)
        return ErrorMessage(
            url = getRequestUri(),
            time = OffsetDateTime.now(),
            message = throwable?.message ?: ""
        )
    }

    data class ErrorMessage(
        val url: String = "",
        val time: OffsetDateTime = OffsetDateTime.now(),
        val message: String = ""
    )
}

class ServerException(message: String) : RuntimeException(message)
