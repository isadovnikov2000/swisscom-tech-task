package com.isadounikau.migration.configuration

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.isadounikau.migration.utils.Constants
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import java.io.InputStream

@Component
class Parsers(
    objectMapper: ObjectMapper
) {
    private val yamlObjectMapper: ObjectMapper
    private val parsers: Map<String, ObjectMapper>

    init {
        yamlObjectMapper = YAMLMapper()
        yamlObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

        parsers = mapOf(
            MediaType.TEXT_PLAIN_VALUE to JavaPropsMapper(),
            MediaType.APPLICATION_JSON_VALUE to objectMapper,
            Constants.TEXT_YAML to yamlObjectMapper
        )
    }

    fun serialize(inputStream: InputStream, contentType: String): JsonNode {
        return parsers[contentType]?.readTree(inputStream) ?: throw IllegalArgumentException("Unsupported content type [$contentType]")
    }

    fun deserialize(obj: Any, contentType: String): ByteArray {
        return parsers[contentType]?.writeValueAsBytes(obj) ?: throw IllegalArgumentException("Unsupported content type [$contentType]")
    }
}


