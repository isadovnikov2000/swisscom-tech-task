package com.isadounikau.migration.utils

import com.fasterxml.jackson.databind.JsonNode

fun JsonNode.getOrNull(fieldName: String): JsonNode? {
    return this.get(fieldName) ?: null
}
