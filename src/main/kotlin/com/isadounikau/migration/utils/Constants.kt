package com.isadounikau.migration.utils

object Constants {
    //https://mailarchive.ietf.org/arch/msg/media-types/e9ZNC0hDXKXeFlAVRWxLCCaG9GI/
    const val TEXT_YAML = "text/vnd.yaml"
}
