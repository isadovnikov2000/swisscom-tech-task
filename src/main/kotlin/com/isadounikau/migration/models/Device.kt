package com.isadounikau.migration.models

typealias DeviceId = String

data class Device(
    val id: DeviceId,
    val model: Model,
    val parameters: Parameters
)

enum class Model {
    ONE, TWO
}

class Parameters : HashMap<String, Any>() {
    fun setParam1(value: Any) {
        this[PARAM_1_NAME] = value
    }

    fun setParam2(value: Any) {
        this[PARAM_2_NAME] = value
    }

    fun setParam3(value: Any) {
        this[PARAM_3_NAME] = value
    }

    fun getParam1(): Any? {
        return this[PARAM_1_NAME]
    }

    fun getParam2(): Any? {
        return this[PARAM_2_NAME]
    }

    fun getParam3(): Any? {
        return this[PARAM_3_NAME]
    }

    companion object {
        private const val PARAM_1_NAME = "param1"
        private const val PARAM_2_NAME = "param2"
        private const val PARAM_3_NAME = "param3"
    }
}
