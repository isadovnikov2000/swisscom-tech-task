package com.isadounikau.migration.models

data class Network(
    val type: NetworkType,
    val deviceId: DeviceId,
    val configuration: Map<String, Any?>
)

enum class NetworkType {
    A, B, C
}
