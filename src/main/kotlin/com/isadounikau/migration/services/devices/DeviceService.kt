package com.isadounikau.migration.services.devices

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.ListenableFutureTask
import com.isadounikau.migration.clients.devices.DeviceApiClient
import com.isadounikau.migration.controllers.DeviceController
import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.DeviceId
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.services.networks.NetworkService
import mu.KotlinLogging
import org.springframework.cache.annotation.CacheEvict
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.temporal.ChronoUnit

private val logger = KotlinLogging.logger { }

interface DeviceService {
    fun save(device: Device)
    fun getNetworksByDeviceId(id: DeviceId): Set<Network>
}

@Service
class DefaultDeviceService(
    private val networkService: NetworkService,
    deviceNetworksStorageCacheLoader: DeviceNetworksStorageCacheLoader
) : DeviceService {

    private val deviceNetworksStorage: LoadingCache<DeviceId, Set<Network>> = CacheBuilder.newBuilder()
        .refreshAfterWrite(Duration.of(1, ChronoUnit.HOURS))
        .build(deviceNetworksStorageCacheLoader)

    @CacheEvict(DeviceController.DEVICE_NETWORKS_CACHE, key = "#device.id")
    override fun save(device: Device) {
        logger.debug { "Save device [$device]" }
        val networks = networkService.getNetworksByDevice(device)
        deviceNetworksStorage.put(device.id, networks)
    }

    override fun getNetworksByDeviceId(id: DeviceId): Set<Network> {
        logger.debug { "Get networks [$DeviceId]" }
        return deviceNetworksStorage[id] ?: throw NetworkNotFoundException("Networks for device [$id] not found")
    }

}

@Component
class DeviceNetworksStorageCacheLoader(
    private val networkService: NetworkService,
    private val deviceApiClient: DeviceApiClient
) : CacheLoader<DeviceId, Set<Network>>() {

    @CacheEvict(DeviceController.DEVICE_NETWORKS_CACHE, key = "#key")
    override fun reload(key: DeviceId, oldValue: Set<Network>): ListenableFuture<Set<Network>> {
        return ListenableFutureTask.create {
            try {
                this.load(key)
            } catch (ex: Exception) {
                logger.warn("Exception thrown during refresh driver info with id [$key]")
                throw ex
            }
        }
    }

    @CacheEvict(DeviceController.DEVICE_NETWORKS_CACHE, key = "#key")
    override fun load(key: DeviceId): Set<Network> {
        try {
            val device = deviceApiClient.getDevice(key)
            return networkService.getNetworksByDevice(device)
        } catch (e: Exception) {
            logger.error("Exception during requesting driver info", e)
            throw e
        }
    }
}

class NetworkNotFoundException(message: String) : RuntimeException(message)
