package com.isadounikau.migration.services.networks

import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.services.networks.mapping.DeviceToNetworkMapperAdapter
import org.springframework.stereotype.Service

interface NetworkService {
    fun getNetworksByDevice(device: Device): Set<Network>
}

@Service
class DefaultNetworkService(
    private val deviceToNetworkMapperAdapter: DeviceToNetworkMapperAdapter
) : NetworkService {
    override fun getNetworksByDevice(device: Device): Set<Network> {
        return deviceToNetworkMapperAdapter.mapToNetworks(device)
    }
}
