package com.isadounikau.migration.services.networks.mapping

import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.services.networks.mapping.strategies.DeviceToNetworkStrategy
import com.isadounikau.migration.services.networks.mapping.strategies.ModelOneToNetworkAStrategy
import com.isadounikau.migration.services.networks.mapping.strategies.ModelOneToNetworkBStrategy
import com.isadounikau.migration.services.networks.mapping.strategies.ModelTwoToNetworkAStrategy
import com.isadounikau.migration.services.networks.mapping.strategies.ModelTwoToNetworkBStrategy
import com.isadounikau.migration.services.networks.mapping.strategies.ModelTwoToNetworkCStrategy
import org.springframework.stereotype.Component
import kotlin.reflect.full.createInstance

@Component
class DeviceToNetworkMapperAdapter {

    //use reflection for lazy import
    private val mappers = setOf(
        ModelOneToNetworkAStrategy(),
        ModelOneToNetworkBStrategy(),
        ModelTwoToNetworkAStrategy(),
        ModelTwoToNetworkBStrategy(),
        ModelTwoToNetworkCStrategy()
    )

    fun mapToNetworks(device: Device): Set<Network> {
        return mappers.filter {
            it.applicable(device)
        }.map {
            it.map(device)
        }.toSet()
    }
}


