package com.isadounikau.migration.services.networks.mapping.strategies

import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Model
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.models.NetworkType

interface DeviceToNetworkStrategy {
    fun map(device: Device): Network
    fun applicable(device: Device): Boolean
}
