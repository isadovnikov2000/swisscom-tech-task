package com.isadounikau.migration.services.networks.mapping.strategies

import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Model
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.models.NetworkType

class ModelTwoToNetworkAStrategy : DeviceToNetworkStrategy {

    override fun map(device: Device): Network {
        val deviceId = device.id
        val networkType = NetworkType.A
        val configurations = mapOf(
            "paramA1" to device.parameters.getParam1(),
            "paramA2" to "0"
        )

        return Network(networkType, deviceId, configurations)
    }

    override fun applicable(device: Device): Boolean {
        return device.model == Model.TWO
            && device.parameters.getParam1() != null
            && device.parameters.getParam2() == null
    }
}

