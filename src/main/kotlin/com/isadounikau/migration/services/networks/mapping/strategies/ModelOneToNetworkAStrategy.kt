package com.isadounikau.migration.services.networks.mapping.strategies

import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Model
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.models.NetworkType

class ModelOneToNetworkAStrategy : DeviceToNetworkStrategy {

    override fun map(device: Device): Network {
        val deviceId = device.id
        val networkType = NetworkType.A
        val configurations = mapOf(
            "paramA1" to device.parameters.getParam1()
        )

        return Network(networkType, deviceId, configurations)
    }

    override fun applicable(device: Device): Boolean {
        return device.model == Model.ONE && device.parameters.getParam1() != null
    }

}
