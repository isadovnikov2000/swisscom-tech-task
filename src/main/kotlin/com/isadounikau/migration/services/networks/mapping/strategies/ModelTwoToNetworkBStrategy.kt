package com.isadounikau.migration.services.networks.mapping.strategies

import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Model
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.models.NetworkType

class ModelTwoToNetworkBStrategy : DeviceToNetworkStrategy {

    override fun map(device: Device): Network {
        val deviceId = device.id
        val networkType = NetworkType.B
        val configurations = mapOf(
            "configB1" to device.parameters.getParam1(),
            "configB2" to device.parameters.getParam2()
        )

        return Network(networkType, deviceId, configurations)
    }

    override fun applicable(device: Device): Boolean {
        return device.model == Model.TWO
            && device.parameters.getParam2() != null
    }
}

