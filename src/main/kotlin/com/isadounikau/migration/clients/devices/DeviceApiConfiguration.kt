package com.isadounikau.migration.clients.devices

import feign.Feign
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DeviceApiConfiguration(
    @Value("\${deviceStorage.api.url}") private val url: String
) {

    @Bean
    fun deviceApi(): DeviceApi = Feign.builder().target(DeviceApi::class.java, url)

}
