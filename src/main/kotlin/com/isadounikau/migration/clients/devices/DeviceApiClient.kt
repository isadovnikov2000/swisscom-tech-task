package com.isadounikau.migration.clients.devices

import com.isadounikau.migration.configuration.Parsers
import com.isadounikau.migration.configuration.ServerException
import com.isadounikau.migration.controllers.mappers.DeviceMapperAdapter
import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.DeviceId
import mu.KotlinLogging
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

private val logger = KotlinLogging.logger { }

@Service
class DeviceApiClient(
    private val deviceApi: DeviceApi,
    private val parsers: Parsers,
    private val deviceMapperAdapter: DeviceMapperAdapter
) {

    fun getDevice(deviceId: DeviceId): Device {
        val response = deviceApi.getDevice(deviceId)
        return when (response.status()) {
            in HttpStatus.OK.value()..HttpStatus.BAD_REQUEST.value() -> {
                val contentType = response.headers()[HttpHeaders.CONTENT_TYPE]!!.first()
                val inputStream = response.body().asInputStream()
                parsers.serialize(inputStream, contentType).let { deviceMapperAdapter.mapJsonNodeToDevice(it) }
            }
            HttpStatus.NOT_FOUND.value() -> throw DeviceNotFoundException("Device [$deviceId] not found")
            else -> {
                logger.error("Unexpected Exception: $response")
                throw ServerException("Unexpected Exception")
            }
        }
    }

}

class DeviceNotFoundException(message: String) : RuntimeException(message)

