package com.isadounikau.migration.clients.devices

import com.isadounikau.migration.models.DeviceId
import feign.Param
import feign.RequestLine
import feign.Response

interface DeviceApi {
    @RequestLine("GET /api/v1/devices/{id}")
    fun getDevice(@Param("id") id: DeviceId): Response
}


