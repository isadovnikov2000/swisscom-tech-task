package com.isadounikau.migration.controllers

import com.isadounikau.migration.configuration.Parsers
import com.isadounikau.migration.controllers.mappers.DeviceMapperAdapter
import com.isadounikau.migration.controllers.mappers.NetworkMapperAdapter
import com.isadounikau.migration.services.devices.DeviceService
import com.isadounikau.migration.utils.Constants.TEXT_YAML
import io.swagger.annotations.ApiImplicitParam
import org.springframework.cache.annotation.Cacheable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/api/v1/devices")
class DeviceController(
    private val parsers: Parsers,
    private val deviceService: DeviceService,
    private val deviceMapperAdapter: DeviceMapperAdapter,
    private val networkMapperAdapter: NetworkMapperAdapter
) {

    @ApiImplicitParam(name = "device", value = "put device configuration here", paramType = "body")
    @PutMapping(consumes = [MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_JSON_VALUE])
    fun saveDevice(request: HttpServletRequest) = request.inputStream.use {
        parsers.serialize(it, request.contentType)
    }.let {
        deviceMapperAdapter.mapJsonNodeToDevice(it)
    }.also {
        deviceService.save(it)
    }

    @Cacheable(DEVICE_NETWORKS_CACHE, key = "#deviceId")
    @GetMapping("/{deviceId}/networks")
    fun getNetworksForDevice(
        @PathVariable deviceId: String
    ): ResponseEntity<ByteArray> {
        return deviceService.getNetworksByDeviceId(deviceId).map {
            networkMapperAdapter.mapNetworkToNetworkResponse(it)
        }.let {
            parsers.deserialize(it, TEXT_YAML)
        }.let { body ->
            ResponseEntity(body, HttpStatus.OK)
        }
    }

    companion object {
        const val DEVICE_NETWORKS_CACHE = "device_networks_cache"
    }

}
