package com.isadounikau.migration.controllers.mappers.strategies.devices

import com.fasterxml.jackson.databind.JsonNode
import com.isadounikau.migration.controllers.mappers.strategies.devices.DeviceMapperStrategy
import com.isadounikau.migration.models.Device
import com.isadounikau.migration.models.Model
import com.isadounikau.migration.models.Parameters
import com.isadounikau.migration.utils.getOrNull

class ModelTwoDeviceMapperStrategy : DeviceMapperStrategy {

    val ID_KEY = "uuid"

    override fun map(node: JsonNode): Device {
        val id = node.get(ID_KEY).asText()

        val param1 = node.getOrNull("param1")
        val param2 = node.getOrNull("param2")
        val param3 = node.getOrNull("param3")

        val parameters = Parameters()
        if (param1 != null) {
            parameters.setParam1(param1.asText())
        }
        if (param2 != null) {
            parameters.setParam2(param2.asText())
        }
        if (param3 != null) {
            parameters.setParam3(param3.asText())
        }

        return Device(id, Model.TWO, parameters)
    }

    override fun applicable(node: JsonNode): Boolean {
        return node.get(ID_KEY) != null
    }
}
