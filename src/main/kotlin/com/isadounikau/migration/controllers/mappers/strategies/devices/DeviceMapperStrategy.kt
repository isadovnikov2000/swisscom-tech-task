package com.isadounikau.migration.controllers.mappers.strategies.devices

import com.fasterxml.jackson.databind.JsonNode
import com.isadounikau.migration.models.Device

interface DeviceMapperStrategy {
    fun map(node: JsonNode): Device
    fun applicable(node: JsonNode): Boolean
}
