package com.isadounikau.migration.controllers.mappers.strategies.networks

import com.isadounikau.migration.controllers.dtos.NetworkResponse
import com.isadounikau.migration.controllers.dtos.NetworkResponseA
import com.isadounikau.migration.models.Network

class NetworkAMapperStrategy: NetworkMapperStrategy {
    override fun map(network: Network): NetworkResponse {
        return NetworkResponseA(network.deviceId, network.configuration)
    }
}
