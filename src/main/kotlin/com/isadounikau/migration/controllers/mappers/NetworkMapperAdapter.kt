package com.isadounikau.migration.controllers.mappers

import com.isadounikau.migration.controllers.dtos.NetworkResponse
import com.isadounikau.migration.controllers.mappers.strategies.networks.NetworkAMapperStrategy
import com.isadounikau.migration.controllers.mappers.strategies.networks.NetworkBMapperStrategy
import com.isadounikau.migration.controllers.mappers.strategies.networks.NetworkCMapperStrategy
import com.isadounikau.migration.models.Network
import com.isadounikau.migration.models.NetworkType
import org.springframework.stereotype.Component

@Component
class NetworkMapperAdapter {

    private val networkMappers = mapOf(
        NetworkType.A to NetworkAMapperStrategy(),
        NetworkType.B to NetworkBMapperStrategy(),
        NetworkType.C to NetworkCMapperStrategy()
    )

    fun mapNetworkToNetworkResponse(network: Network): NetworkResponse {
        return networkMappers[network.type]?.map(network)?: throw NetworkMapperNotFoundException("Network mapper not found for type [${network.type}]")
    }
}

class NetworkMapperNotFoundException(message: String): RuntimeException(message)
