package com.isadounikau.migration.controllers.mappers.strategies.networks

import com.isadounikau.migration.controllers.dtos.NetworkResponse
import com.isadounikau.migration.controllers.dtos.NetworkResponseC
import com.isadounikau.migration.models.Network

class NetworkCMapperStrategy: NetworkMapperStrategy {
    override fun map(network: Network): NetworkResponse {
        return NetworkResponseC(network.deviceId, network.configuration)
    }
}
