package com.isadounikau.migration.controllers.mappers.strategies.networks

import com.isadounikau.migration.controllers.dtos.NetworkResponse
import com.isadounikau.migration.controllers.dtos.NetworkResponseB
import com.isadounikau.migration.models.Network

class NetworkBMapperStrategy: NetworkMapperStrategy {
    override fun map(network: Network): NetworkResponse {
        return NetworkResponseB(network.deviceId, network.configuration)
    }
}
