package com.isadounikau.migration.controllers.mappers

import com.fasterxml.jackson.databind.JsonNode
import com.isadounikau.migration.controllers.mappers.strategies.devices.ModelOneDeviceMapperStrategy
import com.isadounikau.migration.controllers.mappers.strategies.devices.ModelTwoDeviceMapperStrategy
import com.isadounikau.migration.models.Device
import org.springframework.stereotype.Component

@Component
class DeviceMapperAdapter {

    private val deviceMappers = setOf(
        ModelOneDeviceMapperStrategy(),
        ModelTwoDeviceMapperStrategy()
    )

    fun mapJsonNodeToDevice(jsonNode: JsonNode): Device {
        val applicableMappers = deviceMappers.filter {
            it.applicable(jsonNode)
        }
        val size = applicableMappers.size
        return when {
            size == 1 -> applicableMappers.first().map(jsonNode)
            size > 1 -> {
                throw MoreThenOneDeviceMapperFoundException("More then one mapper applicable for Device [$jsonNode], mappers [$applicableMappers]")
            }
            else -> {
                throw NoDeviceMapperFoundException("No mappers found applicable for Device [$jsonNode]")
            }
        }
    }
}

class NoDeviceMapperFoundException(message: String): RuntimeException(message)
class MoreThenOneDeviceMapperFoundException(message: String): RuntimeException(message)

