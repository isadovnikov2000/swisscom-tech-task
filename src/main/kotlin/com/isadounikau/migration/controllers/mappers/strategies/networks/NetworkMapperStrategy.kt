package com.isadounikau.migration.controllers.mappers.strategies.networks

import com.isadounikau.migration.controllers.dtos.NetworkResponse
import com.isadounikau.migration.models.Network

interface NetworkMapperStrategy {
    fun map(network: Network): NetworkResponse
}
