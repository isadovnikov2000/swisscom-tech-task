package com.isadounikau.migration.controllers.dtos

import com.fasterxml.jackson.annotation.JsonProperty
import com.isadounikau.migration.models.DeviceId
import com.isadounikau.migration.models.NetworkType

sealed class NetworkResponse(
    @field:JsonProperty("network_service_type")
    val networkServiceType: NetworkType
)

data class NetworkResponseA(
    @field:JsonProperty("device_id")
    val deviceId: DeviceId,
    val parameters: Map<String, Any?>
) : NetworkResponse(NetworkType.A)

data class NetworkResponseB(
    val device: DeviceId,
    val configuration: Map<String, Any?>
) : NetworkResponse(NetworkType.B)

data class NetworkResponseC(
    val dev: DeviceId,
    val conf: Map<String, Any?>
) : NetworkResponse(NetworkType.C)
